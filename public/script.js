
function calculate()
{
    var prestigeStart, prestigeEnd, prestigeDifference,
    prestigeStart = parseInt(document.getElementById("prestige_start").value);
    prestigeEnd = parseInt(document.getElementById("prestige_end").value);
    
    prestigeDifference = (prestigeEnd - prestigeStart) * 27;
    
    var masterStart, masterEnd, masterDifference,
    masterStart = parseInt(document.getElementById("master_start").value);
    masterEnd = parseInt(document.getElementById("master_end").value);
    
    masterDifference = (masterEnd - masterStart) * 2727;
    
    var championStart, championEnd, championDifference,
    championStart = parseInt(document.getElementById("champion_start").value);
    championEnd = parseInt(document.getElementById("champion_end").value);
    
    championDifference = (championEnd - championStart) * 275427;
    
    var knightStart, knightEnd, knightDifference,
    knightStart = parseInt(document.getElementById("knight_start").value);
    knightEnd = parseInt(document.getElementById("knight_end").value);
    
    knightDifference = (knightEnd - knightStart) * 27818127;
    
    var rankStart, rankStartNumber, rankEnd, rankEndNumber
    rankStart = document.getElementById("rank_start").value;
    rankEnd = document.getElementById("rank_end").value;
    
    switch (rankStart) {
		case 'A':
			rankStartNumber = 1;
			break;
		case 'B':
			rankStartNumber = 2;
			break;
		case 'C':
			rankStartNumber = 3;
			break;	
		case 'D':
			rankStartNumber = 4;
			break;
		case 'E':
			rankStartNumber = 5;
			break;
		case 'F':
			rankStartNumber = 6;
			break;
		case 'G':
			rankStartNumber = 7;
			break;
		case 'H':
			rankStartNumber = 8;
			break;
		case 'I':
			rankStartNumber = 9;
			break;
		case 'J':
			rankStartNumber = 10;
			break;
		case 'K':
			rankStartNumber = 11;
			break;
		case 'L':
			rankStartNumber = 12;
			break;
		case 'M':
			rankStartNumber = 13;
			break;
		case 'N':
			rankStartNumber = 14;
			break;
		case 'O':
			rankStartNumber = 15;
			break;
		case 'P':
			rankStartNumber = 16;
			break;
		case 'Q':
			rankStartNumber = 17;
			break;
		case 'R':
			rankStartNumber = 18;
			break;
		case 'S':
			rankStartNumber = 19;
			break;
		case 'T':
			rankStartNumber = 20;
			break;
		case 'U':
			rankStartNumber = 21;
			break;
		case 'V':
			rankStartNumber = 22;
			break;
		case 'W':
			rankStartNumber = 23;
			break;
		case 'X':
			rankStartNumber = 24;
			break;
		case 'Y':
			rankStartNumber = 25;
			break;
		case 'Z':
			rankStartNumber = 26;
			break;
		case 'FREE':
		case 'Free':
			rankStartNumber = 27;
			break;
	}
	switch (rankEnd) {
		case 'A':
			rankEndNumber = 1;
			break;
		case 'B':
			rankEndNumber = 2;
			break;
		case 'C':
			rankEndNumber = 3;
			break;	
		case 'D':
			rankEndNumber= 4;
			break;
		case 'E':
			rankEndNumber = 5;
			break;
		case 'F':
			rankEndNumber = 6;
			break;
		case 'G':
			rankEndNumber = 7;
			break;
		case 'H':
			rankEndNumber = 8;
			break;
		case 'I':
			rankEndNumber = 9;
			break;
		case 'J':
			rankEndNumber = 10;
			break;
		case 'K':
			rankEndNumber = 11;
			break;
		case 'L':
			rankEndNumber = 12;
			break;
		case 'M':
			rankEndNumber = 13;
			break;
		case 'N':
			rankEndNumber = 14;
			break;
		case 'O':
			rankEndNumber = 15;
			break;
		case 'P':
			rankEndNumber = 16;
			break;
		case 'Q':
			rankEndNumber = 17;
			break;
		case 'R':
			rankEndNumber = 18;
			break;
		case 'S':
			rankEndNumber = 19;
			break;
		case 'T':
			rankEndNumber = 20;
			break;
		case 'U':
			rankEndNumber = 21;
			break;
		case 'V':
			rankEndNumber = 22;
			break;
		case 'W':
			rankEndNumber = 23;
			break;
		case 'X':
			rankEndNumber = 24;
			break;
		case 'Y':
			rankEndNumber = 25;
			break;
		case 'Z':
			rankEndNumber = 26;
			break;
		case 'FREE':
		case 'Free':
			rankEndNumber = 27;
			break;
	}
    var rankDifference = rankEndNumber - rankStartNumber;
    

    var totalDifference = rankDifference + prestigeDifference + masterDifference + championDifference + knightDifference;
    document.getElementById("result").innerHTML = totalDifference;
}
